/* See LICENSE file for copyright and license details. */
/* Default settings; can be overriden by command line. */

static int topbar = 1;                      /* -b  option; if 0, dmenu appears at bottom     */
static int centered = 1;                    /* -c option; centers dmenu on screen */
static int min_width = 1000;                    /* minimum width when centered */
static int fuzzy = 1;                      /* -F  option; if 0, dmenu doesn't use fuzzy matching     */
/* -fn option overrides fonts[0]; default X11 font or font set */
static const char *fonts[] = {
	//"JetBrains Mono ExtraLight:size=10"
	"Iosevka:style=Light:size=9"
};
static const char *prompt      = NULL;      /* -p  option; prompt to the left of input field */


#define DRACULA_COLORS { \
        [SchemeNorm] = { "#bd93f9", "#282a36" },            \
        [SchemeSel] = { "#f8f8f2", "#6272a4" },             \
        [SchemeSelHighlight] = { "#f1fa8c", "#6272a4" },    \
        [SchemeNormHighlight] = { "#f1fa8c", "#282a36" },   \
        [SchemeOut] = { "#44475a", "#8be9fd" },             \
    }

#define GRUVBOX_COLORS { \
        [SchemeNorm] = { "#ebdbb2", "#3c3836" },            \
        [SchemeSel] = { "#fbf1c7", "#504945" },             \
        [SchemeSelHighlight] = { "#fabd2f", "#504945" },    \
        [SchemeNormHighlight] = { "#fabd2f", "#3c3836" },   \
        [SchemeOut] = { "#44475a", "#83a59a" },             \
    }

static const char *colors[SchemeLast][2] = GRUVBOX_COLORS;
/* -l option; if nonzero, dmenu uses vertical list with given number of lines */
static unsigned int lines      = 10;

/*
 * Characters not considered part of a word while deleting words
 * for example: " /?\"&[]"
 */
static const char worddelimiters[] = " ";

/* Size of the window border */
static const unsigned int border_width = 0;

static const unsigned int bar_height = 21;
